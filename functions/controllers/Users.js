'use strict';

var utils = require('../utils/writer.js');
var Users = require('../service/UsersService');

module.exports.usersByUserIdGet = function usersByUserIdGet (req, res, next) {
  var userId = req.swagger.params['userId'].value;
  Users.usersByUserIdGet(userId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.usersGet = function usersGet (req, res, next) {
  Users.usersGet()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
