'use strict';

var utils = require('../utils/writer.js');
var Space = require('../service/SpaceService');

module.exports.spaceAssetsBySpaceIdAndAssetIdGet = function spaceAssetsBySpaceIdAndAssetIdGet (req, res, next) {
  var spaceId = req.swagger.params['spaceId'].value;
  var assetId = req.swagger.params['assetId'].value;
  Space.spaceAssetsBySpaceIdAndAssetIdGet(spaceId,assetId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.spaceAssetsBySpaceIdGet = function spaceAssetsBySpaceIdGet (req, res, next) {
  var spaceId = req.swagger.params['spaceId'].value;
  var contentType = req.swagger.params['contentType'].value;
  Space.spaceAssetsBySpaceIdGet(spaceId,contentType)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response, 404);
    });
};

module.exports.spaceBySpaceIdGet = function spaceBySpaceIdGet (req, res, next) {
  var spaceId = req.swagger.params['spaceId'].value;
  Space.spaceBySpaceIdGet(spaceId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response, 404);
    });
};

module.exports.spaceEntriesBySpaceIdAndEntryIdGet = function spaceEntriesBySpaceIdAndEntryIdGet (req, res, next) {
  var spaceId = req.swagger.params['spaceId'].value;
  var entryId = req.swagger.params['entryId'].value;
  Space.spaceEntriesBySpaceIdAndEntryIdGet(spaceId,entryId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.spaceEntriesBySpaceIdGet = function spaceEntriesBySpaceIdGet (req, res, next) {
  var spaceId = req.swagger.params['spaceId'].value;
  Space.spaceEntriesBySpaceIdGet(spaceId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response, 404);
    });
};

module.exports.spaceGet = function spaceGet (req, res, next) {
  Space.spaceGet()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
