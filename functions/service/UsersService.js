'use strict';


/**
 * UsersByUserId
 *
 * userId String 
 * returns String
 **/
exports.usersByUserIdGet = function(userId) {
  return new Promise(function(resolve, reject) {
      var examples = {};
      examples['application/json'] = {
          '4FLrUHftHW3v2BLi9fzfjU': {
              "fields": {
                  "name": "Alana Atlassian",
                  "role": "Author"
              },
              "sys": {
                  "id": "4FLrUHftHW3v2BLi9fzfjU",
                  "type": "User"
              }
          },
          '4FLrUHftHW3v2BLi9fzfjU2': {
              "fields": {
                  "name": "John Doe",
                  "role": "Editor"
              },
              "sys": {
                  "id": "4FLrUHftHW3v2BLi9fzfjU2",
                  "type": "User"
              }
          }
      };

      const result = (userId in examples['application/json'] ? examples['application/json'][userId] : null);

      if(result){
          resolve(result);
      } else {
          //send 404
          reject(new Error('user not found'));
      }
  });
}


/**
 * Users
 * Get a list of users
 *
 * returns String
 **/
exports.usersGet = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "sys" : {
    "type" : "Array"
  },
  "total" : 1,
  "skip" : 0,
  "limit" : 100,
  "items" : [ {
    "fields" : {
      "name" : "Alana Atlassian",
      "role" : "Author"
    },
    "sys" : {
      "id" : "4FLrUHftHW3v2BLi9fzfjU",
      "type" : "User"
    }
  }, {
    "fields" : {
      "name" : "John Doe",
      "role" : "Editor"
    },
    "sys" : {
      "id" : "4FLrUHftHW3v2BLi9fzfjU2",
      "type" : "User"
    }
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

